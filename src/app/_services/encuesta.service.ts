import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../_shared/constants';
import { HttpClient } from '@angular/common/http';
import { Encuesta } from '../_model/Encuesta';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  urlNegocio: string = `${HOST_BACKEND}/api/encuesta`;
  mensajeRegistro = new Subject<string>();

  constructor(private httpClient: HttpClient) { }

  obtenerTodosLosRegistros() {
    return this.httpClient.get<Encuesta[]>(`${this.urlNegocio}/listar`);
  }

  guardarEncuesta(negocio: Encuesta) {
    return this.httpClient.post(`${this.urlNegocio}/registrar`, negocio);
  }

}
