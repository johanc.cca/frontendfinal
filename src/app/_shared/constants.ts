//export const HOST_BACKEND = `http://192.168.99.100:8080`
//export const HOST_BACKEND = `http://192.168.99.100`
//export const HOST_BACKEND = `http://alb-curso-2033969447.us-east-1.elb.amazonaws.com`;
export const HOST_BACKEND = `https://ori7qbzl77.execute-api.us-east-2.amazonaws.com/dev`;
export const TIME_UPDATE_GEOLOCALIZATION = 60000;
export const RADIO = 0.029;
export const ZOOM = 16;
export const TOKEN_NAME = "idToken";
export const REFRESH_TOKEN_NAME = "refreshToken";
export const ACCESS_TOKEN_NAME = "accessToken";
export const PARAM_USUARIO = "usuario";

export const PROFILE_ADMIN = "ROLE_ADMIN";