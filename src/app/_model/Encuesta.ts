export class Encuesta {
    id: number;
    nombres: string;
    apellidos: string;
    edad: number;
    nombreEncuesta: string;
    tipoEncuesta: string;
    opcionEncuesta: string;

    constructor(){}

}