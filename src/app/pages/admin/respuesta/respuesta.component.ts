import { Component, OnInit, ViewChild } from '@angular/core';
import { EncuestaService } from 'src/app/_services/encuesta.service';
import { MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { Encuesta } from 'src/app/_model/Encuesta';

@Component({
  selector: 'app-respuesta',
  templateUrl: './respuesta.component.html',
  styleUrls: ['./respuesta.component.css']
})
export class RespuestaComponent implements OnInit {

  dataSource: MatTableDataSource<Encuesta>;
  totalElementos: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'encuesta', 'nombres', 'lenguaje'];

  constructor(
    private encuestaService: EncuestaService,
    private snackBar: MatSnackBar
  ) { 
    this.dataSource = new MatTableDataSource<Encuesta>();
  }

  ngOnInit() {
    this.snackBar.open("CARGANDO ENCUESTAS!!!", null, {
      duration: 1500,
    });
    this.cargarTabla();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  cargarTabla(){
    this.encuestaService.obtenerTodosLosRegistros().subscribe((datos) => {
      let respuestas = JSON.parse(JSON.stringify(datos)).content;
      console.log(datos);
      this.dataSource = new MatTableDataSource<Encuesta>(datos);
      this.totalElementos = JSON.parse(JSON.stringify(datos)).totalElements;
    });
  }

}
