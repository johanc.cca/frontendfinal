import { Component, OnInit } from '@angular/core';
import { Encuesta } from 'src/app/_model/Encuesta';
import { EncuestaService } from 'src/app/_services/encuesta.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {

  encuestas: Encuesta[] = [];
  encuesta: Encuesta;
  opcionLenguaje: string = 'Java';
  tipoLenguajes = [
    'Java',
    'Javascript',
    'Ruby',
    'Python',
    'Go'
  ];

  constructor(
    private encuestaService: EncuestaService,
    private snackBar: MatSnackBar
  ) { 
    this.encuesta = new Encuesta();
  }

  ngOnInit() {
  }

  enviar() {
    this.encuesta.opcionEncuesta = this.opcionLenguaje;
    this.encuesta.nombreEncuesta = "¿Cuál es tu lenguaje favorito?";
    this.encuesta.tipoEncuesta = "¿Cuál es tu lenguaje favorito?";
    this.encuestaService.guardarEncuesta(this.encuesta).subscribe((data)=>{
      this.snackBar.open("Registrado Correctamente!!!", null, {
        duration: 1500,
      });
      this.encuesta = new Encuesta();
      this.opcionLenguaje = 'Java';
    }, (error) => {
      this.snackBar.open("Error al guardar su encuesta!!!", null, {
        duration: 1500,
      });
    });
  }

}
